//
//  #ClassName#.h
//  #ProjectName#
//
//  Created by Daniel.Liu on 7/26/14.
//
//

#ifndef __#ProjectName#__#ClassName#__
#define __#ProjectName#__#ClassName#__

#include "SBUINode.h"

class #ClassName# : public SBUINode
{
public:
    #ClassName#();
    ~#ClassName#();

    SB_STATIC_NEW_AUTORELEASE_OBJECT_WITH_INIT_METHOD(#ClassName#, create);
    SB_STATIC_CREATENODE2_METHOD(#ClassName#, "#FileName#");

public:
    virtual void                onNodeLoaded(Node *pNode, NodeLoader *pNodeLoader);
    virtual bool                onAssignCCBMemberVariable(Ref *pTarget, const char *pMemberVariableName, Node *pNode);
    virtual bool                onAssignCCBCustomProperty(Ref *pTarget, const char *pMemberVariableName, const Value &pValue);
    virtual SEL_CallFuncN       onResolveCCBCCCallFuncSelector(Ref *pTarget, const char *pSelectorName);
    virtual Control::Handler    onResolveCCBCCControlSelector(Ref *pTarget, const char *pSelectorName);

// actions
private:
#Actions#

// callbacks
private:
#Callbacks#

// variables
private:
#Variables#

// custom properties
private:
#CustomProperties#
};

CLASS_NODE_LOADER2(#ClassName#);

#endif /* defined(__#ProjectName#__#ClassName#__) */


//
//  SBDropBoxView.swift
//  SBCodeGenerator
//
//  Created by LiuHuanMing on 7/27/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

import Cocoa

class SBDropBoxView: NSImageView {
    
    var didPerformDragOperation: ((sender: SBDropBoxView) -> ())?
    
    var fileList = Array<String>()
    
    override init(frame: NSRect) {
        super.init(frame: frame)
    }
    
    required init(coder: NSCoder!) {
        super.init(coder: coder)
    }

    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)
    }
    
    override func draggingEntered(sender: NSDraggingInfo!) -> NSDragOperation {
        return NSDragOperation.Copy
    }
    
    override func prepareForDragOperation(sender: NSDraggingInfo!) -> Bool {
        return true
    }
    
    override func performDragOperation(sender: NSDraggingInfo!) -> Bool  {
        fileList.removeAll(keepCapacity: false)
        
        let pasteboard = sender.draggingPasteboard()
        var pasteboardItems = pasteboard.pasteboardItems
        for item in pasteboardItems {
            fileList.append(item.stringForType("public.file-url"))
        }
        
        didPerformDragOperation?(sender: self)
        
        return true
    }
}

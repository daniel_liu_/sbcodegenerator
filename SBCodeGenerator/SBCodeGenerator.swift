//
//  SBCodeGenerator.swift
//  SBCodeGenerator
//
//  Created by LiuHuanMing on 7/27/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

import Cocoa

enum CustomPropertyType: Int {
    case _Int    = 0
    case _Float  = 1
    case _Bool   = 2
    case _String = 3
    
    func getCastMethodName() -> String {
        switch self {
        case ._Int:             return ".asInt()"
        case ._Float:           return ".asFloat()"
        case ._Bool:            return ".asBool()"
        case ._String:          return ".asString()"
        default:                return ".asUnknown()"
        }
    }
    
    func getCastTypeName() -> String {
        switch self {
        case ._Int:             return "int"
        case ._Float:           return "float"
        case ._Bool:            return "bool"
        case ._String:          return "std::string"
        default:                return "Unknown"
        }
    }
}

enum CCBVarType: String {
    case CCNode             = "CCNode"
    case CCButton           = "CCButton"
    case CCLabelTTF         = "CCLabelTTF"
    case CCNodeGradient     = "CCNodeGradient"
    case CCNodeColor        = "CCNodeColor"
    case CCBFile            = "CCBFile"
    case CCSprite           = "CCSprite"
    case CCSprite9Slice     = "CCSprite9Slice"
    case CCParticleSystem   = "CCParticleSystem"
    case CCLabelBMFont      = "CCLabelBMFont"
    case CCScrollView       = "CCScrollView"
    case CCLayoutBox        = "CCLayoutBox"
    
    func getCodeVarTypeString() -> String {
        switch self {
        case .CCNode:               return "Node"
        case .CCButton:             return "ControlButton"
        case .CCLabelTTF:           return "LabelTTF"
        case .CCNodeGradient:       return "LayerGradient"
        case .CCNodeColor:          return "LayerColor"
        case .CCBFile:              return "CCBFile"
        case .CCSprite:             return "Sprite"
        case .CCSprite9Slice:       return "Scale9Sprite"
        case .CCParticleSystem:     return "ParticleSystemQuad"
        case .CCLabelBMFont:        return "Label"
        case .CCScrollView:         return "ScrollView"
        case .CCLayoutBox:          return "LayoutBox"
        default:                    return "Unknown"
        }
    }
}

class SBCodeGenerator {
    var className: String?
    var callbacks = Array<String>()
    var actions = Array<String>()
    var variables = Dictionary<String, String>()            // type, name
    var customProperties = Dictionary<String, Int>()        // name, type
    var projectName = "SBCodeGenerator"
    var ccbFileName: String?
    
    init(className: String) {
        self.className = className
    }
    
    func addCallback(callback: String) {
        callbacks.append(callback)
    }
    
    func addAction(action: String) {
        actions.append(action)
    }
    
    func addVariable(newKey: CCBVarType, newValue: String) {
        let realKey = newKey.getCodeVarTypeString()
        variables[realKey] = newValue
    }
    
    func addCustomProperty(name: String, type: Int) {
        customProperties[name] = type
    }
    
    func replaceStr(originalStr: String, targetStr: String, newStr: String) -> String {
        return originalStr.stringByReplacingOccurrencesOfString(targetStr, withString: newStr, options: .CaseInsensitiveSearch, range: nil)
    }
    
    func generateSBCodes() {
        //  read header & cpp files
        let templateHeaderPath = NSBundle.mainBundle().pathForResource("SBHeader.h.txt", ofType: nil)
        let templateCppPath = NSBundle.mainBundle().pathForResource("SBCpp.cpp.txt", ofType: nil)
        let headerContents = NSString(contentsOfFile: templateHeaderPath!, encoding: NSUTF8StringEncoding, error: nil)
        let cppContents = NSString(contentsOfFile: templateCppPath!, encoding: NSUTF8StringEncoding, error: nil)
        
        //  class name & project name
        var outputHeaderString = replaceStr(headerContents, targetStr: "#ClassName#", newStr: self.className!)
        outputHeaderString = replaceStr(outputHeaderString, targetStr: "#ProjectName#", newStr: self.projectName)
        var outputCppString = replaceStr(cppContents, targetStr: "#ClassName#", newStr: self.className!)
        outputCppString = replaceStr(outputCppString, targetStr: "#ProjectName#", newStr: self.projectName)
        
        //  ccbi file name
        var ccbiName = self.ccbFileName!.stringByDeletingPathExtension.stringByAppendingPathExtension("ccbi")!
        outputHeaderString = replaceStr(outputHeaderString, targetStr: "#FileName#", newStr: ccbiName)
        
        //  actions
        var idx: Int = 0
        var actionsHeaderStr = String()
        var actionsCppStr1 = String(), actionsCppStr2 = String()
        for action in self.actions {
            var nStr: String = "\n"
            if (self.actions.count - 1) == idx {
                nStr = ""
            }
            actionsHeaderStr += "\tvoid \(action)(Ref *pSender, Control::EventType pControlEvent);\(nStr)"
            actionsCppStr1 += "\tCCB_SELECTORRESOLVER_CCCONTROL_GLUE(this, \"\(action)\", \(self.className!)::\(action));\(nStr)"
            actionsCppStr2 += "void \(self.className!)::\(action)(Ref *pSender, Control::EventType pControlEvent)\n{\n}\n\(nStr)"
            ++idx
        }
        outputHeaderString = replaceStr(outputHeaderString, targetStr: "#Actions#", newStr: actionsHeaderStr)
        outputCppString = replaceStr(outputCppString, targetStr: "#Actions1#", newStr: actionsCppStr1)
        outputCppString = replaceStr(outputCppString, targetStr: "#Actions2#", newStr: actionsCppStr2)
        
        //  callbacks
        idx = 0
        var callbacksHeaderStr = String()
        var callbacksCppStr1 = String(), callbacksCppStr2 = String()
        for callback in self.callbacks {
            var nStr: String = "\n"
            if (self.callbacks.count - 1) == idx {
                nStr = ""
            }
            callbacksHeaderStr += "\tvoid \(callback)(Node *pSender);\(nStr)"
            callbacksCppStr1 += "\tCCB_SELECTORRESOLVER_CALLFUNC_GLUE(this, \"\(callback)\", \(self.className!)::\(callback));\(nStr)"
            callbacksCppStr2 += "void \(self.className!)::\(callback)(Node *pSender)\n{\n}\n\(nStr)"
            ++idx
        }
        outputHeaderString = replaceStr(outputHeaderString, targetStr: "#Callbacks#", newStr: callbacksHeaderStr)
        outputCppString = replaceStr(outputCppString, targetStr: "#Callbacks1#", newStr: callbacksCppStr1)
        outputCppString = replaceStr(outputCppString, targetStr: "#Callbacks2#", newStr: callbacksCppStr2)
        
        //  variables
        idx = 0
        var variablesHeaderStr = String()
        var variablesCppStr1 = String(), variablesCppStr2 = String(), variablesCppStr3 = String()
        for variable in self.variables {
            var nStr: String = "\n"
            if (self.variables.count - 1) == idx {
                nStr = ""
            }
            variablesHeaderStr += "\t\(variable.0)*\t\(variable.1);\(nStr)"
            variablesCppStr1 += "\t\(variable.1) = nullptr;\(nStr)"
            variablesCppStr2 += "\tCC_SAFE_RELEASE(\(variable.1));\(nStr)"
            variablesCppStr3 += "\tSB_MEMBERVARIABLEASSIGNER_GLUE(this, \"\(variable.1)\", \(variable.0)*, \(variable.1));\(nStr)"
        }
        outputHeaderString = replaceStr(outputHeaderString, targetStr: "#Variables#", newStr: variablesHeaderStr)
        outputCppString = replaceStr(outputCppString, targetStr: "#Variables1#", newStr: variablesCppStr1)
        outputCppString = replaceStr(outputCppString, targetStr: "#Variables2#", newStr: variablesCppStr2)
        outputCppString = replaceStr(outputCppString, targetStr: "#Variables3#", newStr: variablesCppStr3)
        
        //  custom properties
        idx = 0
        var customPropertiesHeaderStr = String()
        var customPropertiesCppStr = String()
        for cProperty in self.customProperties {
            var nStr: String = "\n"
            if (self.customProperties.count - 1) == idx {
                nStr = ""
            }
            var type = CustomPropertyType.fromRaw(cProperty.1)!
            customPropertiesHeaderStr += "\t\(type.getCastTypeName())\t\(cProperty.0);\(nStr)"
            customPropertiesCppStr += "\t\tif ( 0 == strcmp(pMemberVariableName, \"\(cProperty.0)\")) { \(cProperty.0) = pValue\(type.getCastMethodName()); }\(nStr)"
        }
        outputHeaderString = replaceStr(outputHeaderString, targetStr: "#CustomProperties#", newStr: customPropertiesHeaderStr)
        outputCppString = replaceStr(outputCppString, targetStr: "#CustomProperties#", newStr: customPropertiesCppStr)
        
        //  write to file, default is Download Folder
        let downloadDirectory = NSSearchPathForDirectoriesInDomains(.DownloadsDirectory, .UserDomainMask, true)
        var headerFilePath = downloadDirectory[0].stringByAppendingPathComponent("\(self.className!).h")
        var cppFilePath = downloadDirectory[0].stringByAppendingPathComponent("\(self.className!).cpp")
        outputHeaderString.writeToFile(headerFilePath, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
        outputCppString.writeToFile(cppFilePath, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
    }
    
    class func generateCodes(ccbFilePath: String, projectName: String?) {
        var generator = SBCodeGenerator.parseCCBFile(ccbFilePath)
        generator.ccbFileName = ccbFilePath.lastPathComponent
        if !projectName!.isEmpty {
            generator.projectName = projectName!
        }
        generator.generateSBCodes()
    }
    
    class func generateCodes(ccbFileUrl: NSURL, projectName: String?) {
        var generator = SBCodeGenerator.parseCCBFile(ccbFileUrl)
        generator.ccbFileName = ccbFileUrl.lastPathComponent
        if !projectName!.isEmpty {
            generator.projectName = projectName!
        }
        generator.generateSBCodes()
    }
    
    class func parseCCBFile(fileUrl: NSURL) -> SBCodeGenerator {
        let fileContents = NSDictionary(contentsOfURL: fileUrl)
        return SBCodeGenerator.parseCCBFileContents(fileContents)
    }
    
    class func parseCCBFile(filePath: String) -> SBCodeGenerator {
        let fileContents = NSDictionary(contentsOfFile: filePath)
        return SBCodeGenerator.parseCCBFileContents(fileContents)
    }
    
    class func getMemberVarAndBlocksByNode(theNode: AnyObject!, ccbConfig: SBCodeGenerator) {
        let children = theNode.objectForKey("children") as [NSDictionary]
        for element in children {
            let baseClassName = element.objectForKey("baseClass") as String
            println(" - baseClass name: \(baseClassName)")
            
            let memberVarAssignmentName = element.objectForKey("memberVarAssignmentName") as String
            if !memberVarAssignmentName.isEmpty {
                ccbConfig.addVariable(CCBVarType.fromRaw(baseClassName)!, newValue: memberVarAssignmentName)
                println(" - memberVarAssignment name: \(memberVarAssignmentName)")
            }
            
            //  only CCButton has blocks(actions)
            if baseClassName == "CCButton" {
                let properties = element.objectForKey("properties") as NSArray
                for prop in properties as [NSDictionary] {
                    let propName = prop.objectForKey("name") as String
                    if propName == "block" {
                        let propValues = prop.objectForKey("value") as NSArray
                        let blockName = propValues[0] as String
                        ccbConfig.addAction(blockName)
                        println(" - block name: \(blockName)")
                    }
                }
            }
            
            //  recursive for every child
            getMemberVarAndBlocksByNode(element, ccbConfig: ccbConfig)
        }
    }
    
    class func parseCCBFileContents(fileContents: AnyObject) -> SBCodeGenerator {
        
        let nodeGraph = fileContents.objectForKey("nodeGraph") as NSDictionary
        let customClassName = nodeGraph.objectForKey("customClass") as String
        if customClassName.isEmpty {
            println(" - Lack of CustomClass Name -")
            exit(-1)
        }
        println(" - customClass name: \(customClassName)")
        
        var ccbConfig = SBCodeGenerator(className: customClassName)
        SBCodeGenerator.getMemberVarAndBlocksByNode(nodeGraph, ccbConfig: ccbConfig)
        
        let customProperties = nodeGraph.objectForKey("customProperties") as? [NSDictionary]
        if let customProperties = customProperties {
            for cProperty in customProperties {
                let cName = cProperty.objectForKey("name") as String
                let cType = cProperty.objectForKey("type") as Int
                ccbConfig.addCustomProperty(cName, type: cType)
                println(" - custom property: \(cName) with type \(cType)")
            }
        }
        
        let sequences = fileContents.objectForKey("sequences") as [NSDictionary]
        for seque in sequences {
            let callbackChannel = seque.objectForKey("callbackChannel") as NSDictionary
            let keyframes = callbackChannel.objectForKey("keyframes") as [NSDictionary]
            if keyframes.count > 0 {
                let frameValues = keyframes[0].objectForKey("value") as [AnyObject]
                let callbackName = frameValues[0] as String
                ccbConfig.addCallback(callbackName)
                println(" - callback name: \(callbackName)")
            }
        }
        
        return ccbConfig
    }
}


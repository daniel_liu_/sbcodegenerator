//
//  AppDelegate.swift
//  SBCodeGenerator
//
//  Created by LiuHuanMing on 7/27/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

import Cocoa

class AppDelegate: NSObject, NSApplicationDelegate {
                            
    @IBOutlet weak var window: NSWindow!

    @IBOutlet weak var dropboxView: SBDropBoxView!
    @IBOutlet weak var projectNameField: NSTextField!
    
    func applicationDidFinishLaunching(aNotification: NSNotification?) {
        dropboxView.didPerformDragOperation = dropboxDidDragged;
    }

    func applicationWillTerminate(aNotification: NSNotification?) {
        // Insert code here to tear down your application
    }
    
    var projectName: String? {
        return projectNameField.stringValue
    }
    
    func dropboxDidDragged(dropbox: SBDropBoxView) {
        var fileList = dropbox.fileList
        for file in fileList {
            var error: NSError? = nil
            var url = NSURL.URLWithString(file, relativeToURL: nil)
            var contents = NSString(contentsOfURL: url, encoding: NSUTF8StringEncoding, error: &error)
            if let err = error {
                println("error: \(err)")
            }
            
            SBCodeGenerator.generateCodes(url, projectName: projectName)
        }
    }

}

